package net.gdface.facelog;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

import org.apache.commons.configuration2.CombinedConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gu.simplemq.Channel;
import net.gdface.facelog.CommonConstant;

/**
 * 服务端常量定义
 * @author guyadong
 *
 */
public interface ServiceConstant extends CommonConstant{
    public static final Logger logger = LoggerFactory.getLogger(ServiceConstant.class);
    
	/** 设备访问令牌表,{@code 设备ID -> token}  */
    public static final Channel<Token> TABLE_DEVICE_TOKEN = new Channel<Token>("DeviceToken"){} ;
    /** 人员访问令牌表 {@code 人员ID -> token} */
    public static final Channel<Token> TABLE_PERSON_TOKEN = new Channel<Token>("PersonToken"){} ;
    /** 设备命令序列号 {@code cmd sn -> 人员ID} */
    public static final Channel<Integer> TABLE_CMD_SN = new Channel<Integer>("CmdSN"){} ;
    /** 设备响应通道 {@code channel -> 人员ID} */
    public static final Channel<Integer> TABLE_ACK_CHANNEL = new Channel<Integer>("AckChannel"){} ;
	/** 临时密码表,{@code 密码 -> TempPwd对象}  */
    public static final Channel<TmpwdTargetInfo> TABLE_TMP_PWD = new Channel<TmpwdTargetInfo>("TempPwd"){} ;
    /** 临时密码表,{@code TempPwd对象->密码}  */
    public static final Channel<String> TABLE_TMP_PWD_R = new Channel<String>("TempPwdR"){} ;
    /////////////  REDIS KEYS ////////////////////////////
	/** redis 全局变量 : 设备命令序列号, 调用{@link TokenMangement#applyCmdSn(int)}时每次加1,保证序列号在全网络唯一 */
	String KEY_CMD_SN = "CMD_SN";
	/** redis 全局变量 : 设备响应通道序列号, 调用{@link TokenMangement#applyAckChannel(int, int)}时每次加1,保证序列号在全网络唯一 */
	String KEY_ACK_SN = "ACK_CHANNEL";
	/** redis 全局常量 : 设备命令通道 */
	String KEY_CMD_CHANNEL = "CMD_CHANNEL";
	/** redis 全局常量 : 人员验证实时监控通道 */
	String KEY_LOG_MONITOR_CHANNEL = "LOG_MONITOR_CHANNEL";
	/** redis 全局常量 : 设备心跳实时监控通道 */
	String KEY_HB_MONITOR_CHANNEL = "HB_MONITOR_CHANNEL";
    ///////////// PROPERTY KEY DEFINITION (不对 client 公开的 key 定义)///////////
	
    /** salt for password */
    public static final String TOKEN_SALT = "token.salt";

	/////////////////////////////////////////////////
	
	/** database 配置属性前缀 */
	public static final String PREFIX_DATABASE = "database.";
	
	/** faceapi 服务配置属性前缀 */
	public static final String PREFIX_FACEAPI_SERVICE="faceapi.service.";
	
	/** faceapi 服务管理参数:定时任务间隔(秒) */
	public static final String FACEAPI_SERVICEMANAGEMENT_TIMERPERIODSEC ="faceapi.management.timerPeriodSec";
	
	/** faceapi 服务管理参数:1:N搜索时默认的相似阀值 */
	public static final String FACEAPI_SERVICEMANAGEMENT_SIMTHRESHOLD ="faceapi.management.simThreshold";

	/** faceapi 服务管理参数:是否异步初始化FSE搜索引擎,default:true */
	public static final String FACEAPI_SERVICEMANAGEMENT_ASYNCINIT ="faceapi.management.asyncInit";
	
	/** faceapi 服务默认主机名 */
	public static final String DEFAULT_FACEAPI_SERVICE_HOST = "127.0.0.1";
	
	/** faceapi 服务默认端口号 */
	public static final int DEFAULT_FACEAPI_SERVICE_PORT = 26421;

	/** faceapi 服务默认定时任务间隔(秒) */
	public static final int DEFAULT_FACEAPI_SERVICEMANAGEMENT_TIMERPERIOD = 60;

	/** faceapi 服务1:N搜索时默认的相似阀值 */
	public static final float DEFAULT_FACEAPI_SERVICEMANAGEMENT_SIMTHRESHOLD = 0.65f;
	
	/** 全局配置参数对象 */
	public static final CombinedConfiguration CONFIG = GlobalConfig.getConfig();
	
	/** 全局线程池(自动退出封装) */
	public static final ExecutorService GLOBAL_EXCEUTOR = ExecutorProvider.getGlobalExceutor();

	/** 定时任务线程池对象(自动退出封装) */
	public static final ScheduledExecutorService TIMER_EXECUTOR = ExecutorProvider.getTimerExecutor();
}
