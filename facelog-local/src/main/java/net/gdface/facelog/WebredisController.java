package net.gdface.facelog;

import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.net.URI;
import java.util.ArrayList;
import java.util.Map;

import com.google.common.base.Joiner;
import com.google.common.base.MoreObjects;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;

import net.gdface.utils.NetworkUtil;

/**
 * 本地webredis服务控制器<br>
 * 如果webredis指定为本机服务，则尝试连接，连接不上则尝试启动
 * @author guyadong
 *
 */
public class WebredisController extends BaseServiceController {
	private final static Predicate<String> RESPONSE_VALIDATOR = new Predicate<String>() {
		
		@Override
		public boolean apply(String input) {
			return null == input ? false : input.startsWith("webredis");
		}
	};
	/** webredis服务参数 */
	private final Map<String, Object> parameters;
	/**
	 * 构造方法
	 * @param prameters webredis连接参数
	 */
	WebredisController(Map<String, Object> prameters) {
		this.parameters = prameters;
		String host = (String) parameters.get(WEBREDIS_HOST);
		int port = (int) parameters.get(WEBREDIS_PORT);
		location = URI.create(String.format("http://%s:%s", host,port));
	}
	
	protected boolean testConnect(){		
		return NetworkUtil.testHttpConnectChecked(location.toString(), RESPONSE_VALIDATOR);
	}
	/** 
	 * 根据提供的参数启动本地 webredis 服务(node.js) 
	 */
	@Override
	protected Process startLocalServer(){
		String nodejsExe = (String) parameters.get(NODEJS_EXE);
		String webredisFile = (String) parameters.get(WEBREDIS_FILE);

		ArrayList<String> args = Lists.newArrayList();
		args.add(nodejsExe);
		args.add(webredisFile);
		// 命令行指定端口
		if(parameters.containsKey(WEBREDIS_PORT)){
			args.add("--port=" + parameters.get(WEBREDIS_PORT));
		}
		/** 优先使用  WEBREDIS_RURL  */
		if(parameters.containsKey(WEBREDIS_RURI)){
			// 命令行指定redis 连接url
			args.add("--rurl=" + parameters.get(WEBREDIS_RURI));
		}else{
			// 命令行指定redis 主机
			if(parameters.containsKey(WEBREDIS_RHOST)){
				args.add("--rhost=" + parameters.get(WEBREDIS_RHOST));
			}
			// 命令行指定redis 端口
			if(parameters.containsKey(WEBREDIS_RPORT)){
				args.add("--rport=" + parameters.get(WEBREDIS_RPORT));
			}
			// 命令行指定redis 连接密码
			if(parameters.containsKey(WEBREDIS_RAUTH)){
				args.add("--rauth=" + parameters.get(WEBREDIS_RAUTH));
			}
			// 命令行指定redis 数据库
			if(parameters.containsKey(WEBREDIS_RDB)){
				args.add("--rdb=" + parameters.get(WEBREDIS_RDB));
			}
		}
		try {
			logger.info("start webredis server(启动webredis服务器) {}",MoreObjects.firstNonNull(parameters.get(WEBREDIS_PORT), DEFAULT_WEBREDIS_PORT));
			String cmd = Joiner.on(' ').join(args);
			logger.debug("cmd(启动命令): {}",cmd);
			return new ProcessBuilder(args)
					.redirectError(Redirect.INHERIT)
					.redirectOutput(Redirect.INHERIT)
					.start();

		} catch (IOException e) {
			throw new RuntimeException(e);
		} 
	}

	@Override
	protected boolean canStartLocal() {
		return parameters.containsKey(NODEJS_EXE) 
				&& parameters.containsKey(WEBREDIS_FILE);
	}

	@Override
	protected boolean isEmbedded() {
		return false;
	}
}
