package net.gdface.facelog;

import gu.dtalk.DeviceInstruction;
import gu.dtalk.engine.ItemEngineRedisImpl;
import gu.dtalk.engine.TaskDispatcher;
import gu.simplemq.Channel;

/**
 * 基于dtalk的服务端任务执行器<br>
 * @author guyadong
 *
 */
class DtalkServiceTaskDispatcher extends TaskDispatcher {
	private final DtalkServiceMenu menu;
	private final ItemEngineRedisImpl itemAdapter;
	DtalkServiceTaskDispatcher(TokenMangement tm,MessageQueueManagement mm) {
		super(-1, mm.getFactory().getPublisher(),mm.getFactory().getConsumer());
		this.menu = new DtalkServiceMenu().init();
		this.itemAdapter = (ItemEngineRedisImpl) new ItemEngineRedisImpl(mm.getFactory().getPublisher()).setRoot(menu);
		setItemAdapter(itemAdapter);
		setCmdSnValidator(tm.cmdSnValidator);
	}
	/**
	 * 当前对象注册到指定的频道
	 * @param name 频道名
	 * @return 当前对象
	 */
	public DtalkServiceTaskDispatcher register(String name){
		Channel<DeviceInstruction> channel = new Channel<>(name,DeviceInstruction.class,this);
		doRegister(channel);
		return this;
	}
	/**
	 * 当前对象从指定的频道注销
	 * @param name 频道名
	 * @return 当前对象
	 */
	public DtalkServiceTaskDispatcher unregister(String name){
		doUnregister(name);
		return this;
	}

}
