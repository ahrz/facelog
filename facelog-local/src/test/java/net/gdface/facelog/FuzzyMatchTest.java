package net.gdface.facelog;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gu.sql2java.StringMatchType;

public class FuzzyMatchTest {
	private static final Logger logger = LoggerFactory.getLogger(FuzzyMatchTest.class);
	private static DaoManagement dm;
	private static FuzzyMatchManagement fuzzyMatch;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dm = new DaoManagement();

		fuzzyMatch = new FuzzyMatchManagement(dm).init();
		
	}

	private static void testSearchDeviceGroup(String pattern){
		try {
			List<MatchEntry> matched = fuzzyMatch.fuzzySearch("fl_device_group",null, pattern, (StringMatchType)null, 0, null, 0);
			if(matched.isEmpty()){
				logger.info("Not match {}",pattern);
			}else{
				logger.info("Match {}",pattern);
				for(MatchEntry entry:matched){
					logger.info("{}",entry);
				}	
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void test() {
		testSearchDeviceGroup("12102");
	}

}
