package net.gdface.facelog;

import org.junit.Test;

import com.google.common.collect.ImmutableMap;

public class InitTopGroupTest {

	@Test
	public void test() {
		try {
			final TopGroupInfo topGroupInfo = new TopGroupInfo();
			topGroupInfo.setName("家天下");
			ImmutableMap<String, String> nodes = ImmutableMap.<String, String>builder()
					.put("1楼/2单元",",202,101/2,102,201,202")
					.put("2楼","8201房,8202房,8203房,8204房")
					.put("","8101房,8102房,8103房,8104房")
					.put("2楼/物业办公室","201,202,203")
					.put("停车场","")
					.put("东门","")
					.put("北门","")
					.build();
			topGroupInfo.setNodes(nodes);
			ImmutableMap<String, String> permits = ImmutableMap.<String, String>builder()
					.put("1楼/保洁组","1楼,*")
					.put("2楼/保洁组","2楼")
					.put("保安组",",东门,北门,停车场")
					.put("","停车场")
					.put("管理组","2楼/物业办公室,201,208")
					.build();
			topGroupInfo.setPermits(permits);
			final DaoManagement dm = new DaoManagement();
			//Managers.setDebug(true);
			dm.initTopGroup(topGroupInfo);
			/*BaseDao.daoRunAsTransaction(new Callable<Integer>(){
				@Override
				public Integer call() throws Exception {
					return dm.initTopGroup(topGroupInfo);
				}});*/	
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
