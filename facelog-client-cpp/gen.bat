@set sh_folder=%~dp0
@pushd %sh_folder%
call erpc_c.bat && ^
call erpc_c_mini.bat && ^
call gen-erpc-proxy.bat && ^
call gen-erpc-proxy-mini.bat && ^
call gen-client-cpp.bat && ^
call gen-decorator-client-cpp.bat
@popd
