#!/bin/bash
# 执行原生的 Makefile 生成erpc核心库和 erpcgen 并安装到当前文件夹下
# 安装编译所需的工具的依赖库
# 编译 erpcgen 需要安装
#apt-cyg install libboost-devel flex bison

sh_folder=$(cd "$(dirname $0)"; pwd -P)
folder_name=$(basename $sh_folder) 
prefix=$sh_folder/dist/erpc-$(g++ -dumpmachine)

# 清除 release 文件夹
[ -e "$prefix" ] && rm -fr $prefix

_os=$(uname -s)
iscygwin=
build_type=release
# erpcgen 的Makefile 有bug,CYGWIN 下 RELEASE 编译导致erpcgen 会报找不到 -lboost_filesystem. 
# 不想改 Makefile 了,所以这里通过判断操作系统类型,为 CYGWIN 时编译 debug 版本
[[ $_os =~ CYGWIN[^[[:space:]]]* ]] && iscygwin="on" && build_type=debug

pushd $sh_folder/erpc/erpc || exit 255
make clean || exit 255

build=release make erpc  || exit 255
build=release make -C erpc_c install PREFIX=$prefix || exit 255

build=$build_type make erpcgen || exit 255
build=$build_type make -C erpcgen install PREFIX=$prefix || exit 255

if [[ -n "$iscygwin" ]]
then
	echo "============COPY DEPENDENCIES LIBRARY ON CYGWIN ==========="
	cp -fv /usr/bin/cygboost_filesystem-*.dll $prefix/bin || exit 255
	cp -fv /usr/bin/cygboost_system-*.dll $prefix/bin || exit 255
	cp -fv /usr/bin/cyggcc_s-seh-1.dll $prefix/bin || exit 255
	cp -fv /usr/bin/cygstdc++-6.dll $prefix/bin || exit 255
	cp -fv /usr/bin/cygwin1.dll $prefix/bin || exit 255
fi
popd
