#!/bin/bash
# 生成IFaceLog接口的client实现代码(CPP)
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd "$sh_folder"
OUT_FOLDER=src/client
if [ -e "$OUT_FOLDER" ] ;
then
    rm "$OUT_FOLDER/*.cpp" 1>/dev/null 2>/dev/null
    rm "$OUT_FOLDER/*.h"   1>/dev/null 2>/dev/null
    rm "$OUT_FOLDER/*.md"  1>/dev/null 2>/dev/null
fi
mvn com.gitee.l0km:codegen-thrift-maven-plugin:generate@client || exit
popd