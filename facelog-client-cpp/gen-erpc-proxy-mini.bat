@rem 生成IFaceLog接口的erpc proxy实现代码(CPP)
@set sh_folder=%~dp0
@pushd %sh_folder%
@set OUT_FOLDER=src\erpc_proxy_mini
@if exist "%OUT_FOLDER%" (
		del  "%OUT_FOLDER%"\facelog_erpc_cast.* >nul 2>nul
		del  "%OUT_FOLDER%"\facelog_erpc_mem.* >nul 2>nul
		del  "%OUT_FOLDER%"\facelog_erpc_output.* >nul 2>nul
		del  "%OUT_FOLDER%"\facelog_server_impl.cpp >nul 2>nul
		del  "%OUT_FOLDER%"\facelog_proxy_server.cpp >nul 2>nul
	)
call mvn com.gitee.l0km:codegen-thrift-maven-plugin:generate@erpc_proxy_mini
@popd