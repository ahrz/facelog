/*
* test_erpcdemo_client.c
*
*  Created on: Apr 14, 2020
*      Author: guyadong
*/


#include <string.h>
#include <iostream>
#include <erpc_client_setup.h>
#include <erpc_port.h>
#include <facelog_erpc_output.h>
#include <facelog_erpc_mem.h>
#include <erpc_setup_client_tcp.h>
#include "ErpcClientCmdParam.h"
#include <ThreadPool.h>
using namespace std;
using namespace gdface;
using namespace net::gdface::utils;

int main(int argc, char *argv[])
{
	ErpcClientConfig param;
	param.parse(argc, argv);
	std::cout <<"======connect facelog eRPC proxy service:" << param.host <<  ":" << param.port << std::endl;

	/* 创建client端传输层对象(TCP) */
	auto transport = erpc_transport_client_tcp_init(param.host.c_str(), param.port,0,0,0);
	auto message_buffer_factory = erpc_mbf_dynamic_init();
	/* 初始化客户端 */
	erpc_client_init(transport, message_buffer_factory);
	std::string errmsg= std::string(256,'\0');	
	{
		/* RPC 调用 fl_version */
		std::string _return(256,0);
		auto resp = fl_version((char*)errmsg.data(), (char*)_return.data());
		cout << "return code:" << resp << endl;
		if(resp){
			cout << "error message:" << errmsg << endl;
		}else{
			/* 输出返回值 */
			cout << "fl_version response:" << _return << endl;
		}
	}
	{
		/* RPC 调用 fl_getPerson */
		PersonBean_t _return;
		init_PersonBean_t_ref(_return);
		auto resp = fl_getPerson((char*)errmsg.data(),&_return,1);
		cout << "return code:" << resp << endl;
		if(resp){
			cout << "error message:" << errmsg << endl;
		}else{
			/* 输出返回值 */
			cout << "fl_getPerson response:" << _return << endl;
		}
		free_PersonBean_t_ref(_return);

	}
	{
		/* RPC 调用 fl_getDevice */
		DeviceBean_t _return;
		init_DeviceBean_t_ref(_return);
		auto resp = fl_getDevice((char*)errmsg.data(),&_return,37);
		cout << "return code:" << resp << endl;
		if(resp){
			cout << "error message:" << errmsg << endl;
		}else{
			/* 输出返回值 */
			cout << "fl_getDevice response:" << _return << endl;
		}
		free_DeviceBean_t_ref(_return);
	}
	{
		/* RPC 调用 fl_getFeature */
		FeatureBean_t _return;
		init_FeatureBean_t_ref(_return);
		auto resp = fl_getFeature((char*)errmsg.data(),&_return,"0184c99527c8da07214f4eb370bc40f7");
//		auto resp = fl_getFeature((char*)errmsg.data(),&_return,nullptr);
		cout << "return code:" << resp << endl;
		if(resp){
			cout << "error message:" << errmsg << endl;
		}else{
			/* 输出返回值 */
			cout << "fl_getFeature response:" << _return << endl;
		}
		free_FeatureBean_t_ref(_return);
	}
	{
		/* RPC 调用 fl_getFeaturesByPersonIdAndSdkVersion */
		list_string_1_t _return;
		init_list_string_1_t_ref(_return);
		auto resp = fl_getFeaturesByPersonIdAndSdkVersion((char*)errmsg.data(),&_return,4,nullptr);
		//auto resp = fl_getFeaturesByPersonIdAndSdkVersion((char*)errmsg.data(),&_return,4,"GFIR10472");
		cout << "return code:" << resp << endl;
		if(resp){
			cout << "error message:" << errmsg << endl;
		}else{
			/* 输出返回值 */
			cout << "fl_getFeaturesByPersonIdAndSdkVersion response:" << _return << endl;
		}
		free_list_string_1_t_ref(_return);
	}
	{
		printf("Start Online and add log!!!!\n");

		Token_t _return_token;
		DeviceBean_t *d_bean= new DeviceBean_t();
		init_DeviceBean_t(d_bean);
		memcpy(d_bean->m_mac, "4aef8842d941", strlen("4aef8842d941"));
		memcpy(d_bean->m_usedSdks, "GFIR10472", strlen("GFIR10472"));
		memcpy(d_bean->m_serialNo, "4aef8842d941", strlen("4aef8842d941"));
		memcpy(d_bean->m_name, "4aef8842d941", strlen("4aef8842d941"));
		d_bean->m_id = 32;
		fl_online((char *)errmsg.data(), &_return_token, d_bean);

		cout << "fl_online response:" << _return_token << endl;

		LogBean_t *up_log =new LogBean_t();
		init_LogBean_t(up_log);
		up_log->m__new = true;
		up_log->m_personId = 4;
		up_log->m_deviceId = d_bean->m_id;
		up_log->m_verifyTime = 1602917242000;
		up_log->m_verifyType = 0;


		auto resp = fl_addLog((char *)errmsg.data(), up_log, &_return_token);

		if (resp)
		{
			cout << "error message:" << errmsg << endl;
		}
		else
		{
			cout << "fl_addLog success!!!!!" << endl;
		}
		free_DeviceBean_t(d_bean,true);
		free_LogBean_t(up_log,true);
	}
	{
		/* RPC 调用 fl_getPersonsPermittedOnDevice */
		list_int32_1_t _return;
		init_list_int32_1_t(&_return);
		auto resp = fl_getPersonsPermittedOnDevice((char*)errmsg.data(), &_return, 32, false, NULL, 0);
		cout << "return code:" << resp << endl;
		if(resp){
			cout << "error message:" << errmsg << endl;
		}else{
			/* 输出返回值 */
			cout << "fl_getFeaturesByPersonIdAndSdkVersion response:" << _return << endl;
		}
		free_list_int32_1_t_ref(_return);
	}
	/* 关闭socket */
	erpc_transport_client_tcp_deinit();
}
