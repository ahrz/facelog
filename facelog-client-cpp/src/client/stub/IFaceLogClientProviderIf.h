// ______________________________________________________
// Generated by codegen - https://gitee.com/l0km/codegen 
// template: thrift_cpp/client/perservice/client_provider_if.vm
// ______________________________________________________ 
#ifndef _IFACE_LOG_CLIENT_PROVIDER_IF_H
#define _IFACE_LOG_CLIENT_PROVIDER_IF_H
#include "IFaceLog.h"
namespace net{namespace gdface{namespace facelog{
/* 
 * ::gdface::IFaceLogClient 实例提供者(thread safe)接口,
 * 为调用者返回一个 IFaceLogClient 实例
 */
class IFaceLogClientProviderIf {
public:
    virtual ~IFaceLogClientProviderIf() {}
    // 返回当前连接的主机
    virtual std::string getHost() const noexcept = 0;
    // 返回当前连接的端口号
    virtual int getPort() const noexcept = 0;
    virtual ::apache::thrift::transport::TTransport& getTransport() const = 0;
    // 重新指定主机和端口号
    virtual void resetClient(const std::string & host, int port) = 0;
    // 设置超时参数
    // @param connTimeout Connect timeout in mills,ignore if less than or equal to 0
    // @param sendTimeout Send timeout in mills,ignore if less than or equal to 0
    // @param recvTimeout Recv timeout in mills,ignore if less than or equal to 0
    virtual void setTimeout(int connTimeout, int sendTimeout, int recvTimeout) = 0;
    virtual ::gdface::IFaceLogClient& get() const = 0;

    ::gdface::IFaceLogClient& operator*() const { return get(); }
    ::gdface::IFaceLogClient& operator()() const { return get(); }
};
} /* namespace facelog */} /* namespace gdface */} /* namespace net */
#endif /** _IFACE_LOG_CLIENT_PROVIDER_IF_H */