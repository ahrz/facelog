#!/bin/bash
# linux 下交叉编译 facelogclient(armhf)
# 需要依赖的工具和库
# sudo apt-get install libboost-all-dev:armhf libssl-dev:armhf

sh_folder=$(cd "$(dirname $0)"; pwd -P)

CXX=arm-linux-gnueabihf-g++
[ -n "$CROSS_COMPILER_PREFIX" ] && CXX=$CROSS_COMPILER_PREFIX/$CXX

[ ! $($CXX -dumpmachine) ] \
    && echo "not install compiler arm-linux-gnueabihf-g++,install please:" \
    && echo "sudo apt-get install g++-arm-linux-gnueabihf" \
    && echo "sudo apt-get install gcc-arm-linux-gnueabihf" \
    && exit 255

TOOLCHAIN_FILE=$sh_folder/cmake/armhf-linux-gnueabihf.toolchain.cmake \
BUILD_TYPE=RELEASE \
CROSS_COMPILER_PREFIX=$CROSS_COMPILER_PREFIX \
MACHINE=$($CXX -dumpmachine) \
RELEASE_PRJ=ON \
$sh_folder/make_unix_makefile.sh
