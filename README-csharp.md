# csharp client

facelog服务提供RESTful风格的Web API.

使用 swagger提供的client代码生成工具`swagger-codegen`生成csharp client代码访问RESTful风格的Web API,将生成的csharp代码编译动态库，供csharp项目调用 

运行swagger-codegen 需要JDK 7(及以上)支持.

## 下载 swagger-codegen

从maven中央仓库下载 swagger-codegen (io.swagger:swagger-codegen-cli:2.4.20) 

下载位置:https://repo1.maven.org/maven2/io/swagger/swagger-codegen-cli/2.4.20/swagger-codegen-cli-2.4.20.jar

	wget https://repo1.maven.org/maven2/io/swagger/swagger-codegen-cli/2.4.20/swagger-codegen-cli-2.4.20.jar

## 生成client代码(csharp)

命令行执行`swagger-codegen-cli`生成csharp代码:

windows:

	java -jar swagger-codegen-cli-2.4.20.jar generate ^
			-i http://facelog.facelib.net:26413/v2/api-docs ^
			-l csharp ^
			--api-package facelog --model-package facelog.model ^
			-o client\facelog\csharp 

`client\facelog\csharp` 为指定生成代码的输出文件夹

linux:

	java -jar swagger-codegen-cli-2.4.20.jar generate \
			-i http://facelog.facelib.net:26413/v2/api-docs \
			-l csharp \
			--api-package facelog --model-package facelog.model \
			-o client/facelog/csharp 


## 调用示例

参见生成代码文件夹下的`README.md`