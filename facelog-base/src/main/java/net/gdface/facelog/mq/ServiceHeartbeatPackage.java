package net.gdface.facelog.mq;

import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Collections2;

import static net.gdface.utils.MiscellaneousUtils.elementsOf;
import net.gdface.facelog.CommonConstant;
import net.gdface.utils.NetworkUtil;


/**
 * 服务心跳包
 * @author guyadong
 *
 */
public class  ServiceHeartbeatPackage implements CommonConstant{
	/** 服务ID(服务每次启动会不一样，消息接收端根据此判断服务是否重启) */
	private int id;
	/** (FRAMED)服务端口 */
	private Integer port;
	/** (XHR)服务端口,如果XHR服务未启动则为{@code null} */
	private Integer xhrPort;
	/** (RESTful)服务端口,如果RESTful服务未启动则为{@code null} */
	private Integer restfulPort;
	/** (eRPC)服务端口,如果erpc proxy服务未启动则为{@code null} */
	private Integer erpcPort;
	/** (eRPC)服务类型,full:全功能服务,mini:裁剪服务如果erpc proxy服务未启动则为{@code null} */
	private String erpcType;
	/** 主机名 */
	private String host;
	/** 绑定的ip地址列表(','号分隔) */
	private String addresses;
	/** 绑定的MAC地址列表(','号分隔) */
	private String macs;
	/** 服务启动时间戳 ISO8601 格式 */
	private String timestamp = new SimpleDateFormat(ISO8601_FORMATTER_STR).format(new Date());
	public ServiceHeartbeatPackage(int id, Integer port, Integer xhrPort, Integer restfulPort, Integer erpcPort, String erpcType, String host) {
		super();
		this.id = id;
		this.port = port;
		this.xhrPort = xhrPort;
		this.restfulPort = restfulPort;
		this.host = host;
		this.erpcPort = erpcPort;
		this.erpcType = erpcType;				
	}
	public ServiceHeartbeatPackage() {
		super();
	}
	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id 要设置的 id
	 * @return 当前对象
	 */
	public ServiceHeartbeatPackage setId(int id) {
		this.id = id;
		return this;
	}
	/**
	 * @return port
	 */
	public Integer getPort() {
		return port;
	}
	/**
	 * @param port 要设置的 port
	 * @return 当前对象
	 */
	public ServiceHeartbeatPackage setPort(Integer port) {
		this.port = port;
		return this;
	}
	/**
	 * @return xhrPort
	 */
	public Integer getXhrPort() {
		return xhrPort;
	}
	/**
	 * @param xhrPort 要设置的 xhrPort
	 * @return 当前对象
	 */
	public ServiceHeartbeatPackage setXhrPort(Integer xhrPort) {
		this.xhrPort = xhrPort;
		return this;
	}

	/**
	 * @return restfulPort
	 */
	public Integer getRestfulPort() {
		return restfulPort;
	}
	/**
	 * @param restfulPort 要设置的 restfulPort
	 */
	public void setRestfulPort(Integer restfulPort) {
		this.restfulPort = restfulPort;
	}
	
	/**
	 * @return erpcPort
	 */
	public Integer getErpcPort() {
		return erpcPort;
	}
	/**
	 * @param erpcPort 要设置的 erpcPort
	 */
	public void setErpcPort(Integer erpcPort) {
		this.erpcPort = erpcPort;
	}
	
	/**
	 * @return erpcType
	 */
	public String getErpcType() {
		return erpcType;
	}
	/**
	 * @param erpcType 要设置的 erpcType
	 */
	public void setErpcType(String erpcType) {
		this.erpcType = erpcType;
	}
	/**
	 * @return host
	 */
	public String getHost() {
		return host;
	}
	/**
	 * @param host 要设置的 host
	 * @return 当前对象
	 */
	public ServiceHeartbeatPackage setHost(String host) {
		this.host = host;
		return this;
	}

	/**
	 * @return addresses
	 */
	public String getAddresses() {
		return addresses;
	}
	/**
	 * @param addresses 要设置的 addresses
	 * @return 当前对象
	 */
	public ServiceHeartbeatPackage setAddresses(String addresses) {
		this.addresses = addresses;
		return this;
	}
	public String getMacs() {
		return macs;
	}
	public void setMacs(String macs) {
		this.macs = macs;
	}
	public ServiceHeartbeatPackage writeAddresses(Collection<InetAddress>addresses){
		if(addresses != null){
			Collection<String> h = Collections2.transform(addresses, new Function<InetAddress, String>() {
				@Override
				public String apply(InetAddress input) {
					return input.getHostAddress();
				}
			});
			setAddresses(Joiner.on(',').join(h));
			Collection<String> m = Collections2.transform(addresses, new Function<InetAddress, String>() {
				@Override
				public String apply(InetAddress input) {
					return NetworkUtil.formatMac(NetworkUtil.getMacAddress(input), "");
				}
			});
			setMacs(Joiner.on(',').join(m));
		}
		return this;
	}
	public List<String> readAddresses(){
		return Strings.isNullOrEmpty(addresses) ? Collections.<String>emptyList() : elementsOf(addresses);
	}
	/**
	 * @return timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}
	/**
	 * @param timestamp 要设置的 timestamp
	 * @return 当前对象
	 */
	public ServiceHeartbeatPackage setTimestamp(String timestamp) {
		this.timestamp = timestamp;
		return this;
	}
	public long readTimestamp(){
		try {
			return Strings.isNullOrEmpty(timestamp) 
					? 0L 
					: new SimpleDateFormat(ISO8601_FORMATTER_STR).parse(timestamp).getTime();
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ServiceHeartbeatPackage [id=");
		builder.append(id);
		builder.append(", ");
		if (port != null) {
			builder.append("port=");
			builder.append(port);
			builder.append(", ");
		}
		if (xhrPort != null) {
			builder.append("xhrPort=");
			builder.append(xhrPort);
			builder.append(", ");
		}
		if (host != null) {
			builder.append("host=");
			builder.append(host);
			builder.append(", ");
		}
		if (addresses != null) {
			builder.append("address=");
			builder.append(addresses);
			builder.append(", ");
		}
		if (timestamp != null) {
			builder.append("timestamp=");
			builder.append(timestamp);
		}
		builder.append("]");
		return builder.toString();
	}

}