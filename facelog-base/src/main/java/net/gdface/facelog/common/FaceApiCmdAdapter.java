package net.gdface.facelog.common;

import net.gdface.annotation.DeriveMethod;
import net.gdface.facelog.CommonConstant;
import net.gdface.sdk.CapacityFieldConstant;
import net.gdface.sdk.FaceApi;
import static com.google.common.base.Preconditions.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.google.common.base.Function;
import com.google.common.base.MoreObjects;
import com.google.common.base.Throwables;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import gu.dtalk.ICmdImmediateAdapter;
import gu.dtalk.exception.CmdExecutionException;
import gu.simplemq.json.JSONObjectDecorator;

/**
 * 将{@link FaceApi}实例封装成{@link ICmdImmediateAdapter}接口<br>
 * 为设备端的SDK版本算法提供远程调用能力<br>
 * 需要 {@code com.gitee.l0km:faceapi-base,com.gitee.l0km:dtalk-base,com.gitee.l0km:codegen-annotations} 依赖库支持
 * @author guyadong
 *
 */
public class FaceApiCmdAdapter implements ICmdImmediateAdapter,CapacityFieldConstant,CommonConstant,DtalkCommonConstants {
	
	
	public static final Function<Method, String> FUN_PORT_GETTER = new Function<Method, String>() {

		@Override
		public String apply(Method method) {
			if(method == null){
				return null;
			}
			String port = method.getName();
			DeriveMethod deriveMethod = method.getAnnotation(DeriveMethod.class);		
			if(deriveMethod != null && deriveMethod.methodSuffix().length > 0){
				return port + deriveMethod.methodSuffix()[0];
			}
			return port;
		}
	};
	private static final ImmutableMap<String, Method> methods =
		Maps.uniqueIndex(Arrays.asList(FaceApi.class.getDeclaredMethods()), FUN_PORT_GETTER);
	private BiMap<String, FaceApi> faceapis = HashBiMap.create();
	
	public static final FaceApiCmdAdapter INSTANCE = new FaceApiCmdAdapter();
	private FaceApiCmdAdapter(){
	}
	public FaceApiCmdAdapter(String sdkVersion,FaceApi faceApi) {
		bindFaceApi(	sdkVersion,faceApi);
	}
	public FaceApiCmdAdapter(Map<String,FaceApi> faceApis) {
		this.faceapis.putAll(MoreObjects.firstNonNull(faceApis, Collections.<String,FaceApi>emptyMap()));
	}
	@Override
	public Object apply(Map<String, Object> input) throws CmdExecutionException {
		JSONObjectDecorator json = new JSONObjectDecorator(input);
		
		final String sdkVersion = json.getStringOrNull(CMD_FACEAPI_SDKVERSION);
		checkArgument(sdkVersion != null,"NOT DEFINED valid sdkVersion");
		final FaceApi instance = faceapis.get(sdkVersion);
		checkArgument(instance != null,"UNSUPPORTED sdkVersion %s",sdkVersion);
		final String name = json.getStringOrNull(CMD_FACEAPI_METHOD);
		checkArgument(name != null,"NOT DEFINED valid method name");		
		final Method method = methods.get(name);
		checkArgument(method != null,"INVALID method name for FaceApi %s",name);

		try {
			JSONArray parameters = MoreObjects.firstNonNull(json.getJSONArray(CMD_FACEAPI_PARAMETERS),new JSONArray());
			Type[] paramTypes = method.getGenericParameterTypes();
			checkArgument(paramTypes.length == parameters.size(),
					"MISMATCH PARAMETER NUMBER for %s,input %s,required %s",name,parameters.size(),paramTypes.length);
			Object[] args = new Object[paramTypes.length];

			// 从CMD_FACEAPI_PARAMETERS中解析参数顺序填充到参数列表中调用faceapi实例
			for(int i = 0;  i < args.length; ++i){
				args[i] = parameters.getObject(i, paramTypes[i]);
				if(paramTypes[i] instanceof Class<?>){
					checkArgument(!((Class<?>)paramTypes[i]).isPrimitive() || args[i] !=null,"arg%s of method %s is primitive type,must not be null",i,name);
				}
			}
			return method.invoke(instance, args);
		} catch (JSONException e) {
			throw new CmdExecutionException(e);
		}catch (IllegalAccessException e) {
			throw new CmdExecutionException(e);
		} catch (IllegalArgumentException e) {
			throw new CmdExecutionException(e);
		} catch (InvocationTargetException e) {
			throw new CmdExecutionException(e.getTargetException());
		} catch (Exception e) {
			Throwables.throwIfUnchecked(e);
			throw new CmdExecutionException(e);
		}
	}
	/**
	 * 绑定{@link FaceApi}实例到指定的sdk版本号
	 * @param sdkVersion sdk版本号
	 * @param faceApi {@link FaceApi}实例
	 */
	public void bindFaceApi(String sdkVersion,FaceApi faceApi){
		faceapis.put(checkNotNull(sdkVersion,"sdkVersion is null"),checkNotNull(faceApi,"faceApi is null"));			
	}
	/**
	 * 绑定{@link FaceApi}实例到指定的sdk版本号,
	 * sdk版本号由{@link FaceApi#sdkCapacity()}方法提供,sdkVersion方法在faceapi 2.1.7版本以后才定义，
	 * 所以依赖于之前版本的算法不可使用此方法
	 * @param faceApi
	 * @see #bindFaceApi(String, FaceApi)
	 */
	public void bindFaceApi(FaceApi faceApi){
		checkArgument(faceApi != null,"faceApi is null");
		bindFaceApi(faceApi.sdkCapacity().get(C_SDK_VERSION), faceApi);;			
	}
	public void unbindFaceApi(String sdkVersion){
		faceapis.remove(checkNotNull(sdkVersion,"sdkVersion is null"));
	}
	public void unbindFaceApi(FaceApi faceApi){
		faceapis.inverse().remove(checkNotNull(faceApi,"faceApi is null"));
	}
	public Set<String> useSdks(){
		return Sets.newHashSet(faceapis.keySet());
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FaceApiCmdAdapter [faceapis=");
		builder.append(faceapis);
		builder.append("]");
		return builder.toString();
	}
}
