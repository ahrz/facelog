package net.gdface.facelog.common;

import net.gdface.facelog.db.FaceBean;

/**
 * 通用提取人脸特征命令执行的返回值类型
 *
 */
public class ExtractFeatureReturn {
	/** 特征数据 */
    private byte[] feature;
    /** 人脸位置信息 */
    private FaceBean faceBean;
	public byte[] getFeature() {
        return feature;
    }

    public void setFeature(byte[] feature) {
        this.feature = feature;
    }

    public FaceBean getFaceBean() {
        return faceBean;
    }

    public void setFaceBean(FaceBean faceBean) {
        this.faceBean = faceBean;
    }
}