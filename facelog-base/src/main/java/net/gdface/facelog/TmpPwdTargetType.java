package net.gdface.facelog;

/**
 * 定义临时密码的作用目标类型
 * @author guyadong
 *
 */
public enum TmpPwdTargetType{
	/** 用户组 */USER_GROUP_ID,
	/** 设备组 */DEVICE_GROUP_ID,
	/** 用户 */USER_ID,
	/** 设备 */DEVICE_ID		
}