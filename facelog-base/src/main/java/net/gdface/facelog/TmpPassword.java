package net.gdface.facelog;

import java.util.Date;
/**
 * 临时密码描述对象<br>
 * 临时密码用于为设备提供临时的访问权限
 * @author guyadong
 *
 */
public class TmpPassword extends TmpwdTargetInfo {
	private String password;
	public TmpPassword() {
	}
	public TmpPassword(TmpPwdTargetType targetType, int targetId, Date expiryDate,String password) {
		super(targetType, targetId, expiryDate);
		this.password = password;
	}
	/**
	 * @return password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password 要设置的 password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof TmpPassword))
			return false;
		TmpPassword other = (TmpPassword) obj;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TmpPwd [password=");
		builder.append(password);
		builder.append(", getTargetType()=");
		builder.append(getTargetType());
		builder.append(", getTargetId()=");
		builder.append(getTargetId());
		builder.append(", getExpiryDate()=");
		builder.append(getExpiryDate());
		builder.append("]");
		return builder.toString();
	}

}
