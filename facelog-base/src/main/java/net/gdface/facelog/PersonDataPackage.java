package net.gdface.facelog;

import java.util.List;

import net.gdface.facelog.db.FeatureBean;
import net.gdface.facelog.db.PersonBean;

/**
 * 用户信息数据包
 * @author guyadong
 *
 */
public class PersonDataPackage {
	private PersonBean personBean;
	private String schedule;
	private String limit;	
	private List<FeatureBean> featureBeans;
	
	public PersonDataPackage() {
		super();
	}

	public PersonDataPackage(PersonBean personBean, String schedule, String limit, List<FeatureBean> featureBeans) {
		super();
		this.personBean = personBean;
		this.schedule = schedule;
		this.limit = limit;
		this.featureBeans = featureBeans;
	}

	public PersonBean getPersonBean() {
		return personBean;
	}
	public void setPersonBean(PersonBean personBean) {
		this.personBean = personBean;
	}
	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public String getLimit() {
		return limit;
	}

	public void setLimit(String limit) {
		this.limit = limit;
	}

	public List<FeatureBean> getFeatureBeans() {
		return featureBeans;
	}
	public void setFeatureBeans(List<FeatureBean> featureBeans) {
		this.featureBeans = featureBeans;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PersonDataPackage [personBean=");
		builder.append(personBean);
		builder.append(", schedule=");
		builder.append(schedule);
		builder.append(", limit=");
		builder.append(limit);
		builder.append(", featureBeans=");
		builder.append(featureBeans);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((featureBeans == null) ? 0 : featureBeans.hashCode());
		result = prime * result + ((limit == null) ? 0 : limit.hashCode());
		result = prime * result + ((personBean == null) ? 0 : personBean.hashCode());
		result = prime * result + ((schedule == null) ? 0 : schedule.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof PersonDataPackage))
			return false;
		PersonDataPackage other = (PersonDataPackage) obj;
		if (featureBeans == null) {
			if (other.featureBeans != null)
				return false;
		} else if (!featureBeans.equals(other.featureBeans))
			return false;
		if (limit == null) {
			if (other.limit != null)
				return false;
		} else if (!limit.equals(other.limit))
			return false;
		if (personBean == null) {
			if (other.personBean != null)
				return false;
		} else if (!personBean.equals(other.personBean))
			return false;
		if (schedule == null) {
			if (other.schedule != null)
				return false;
		} else if (!schedule.equals(other.schedule))
			return false;
		return true;
	}
	
}