// ______________________________________________________
// Generated by sql2java - https://github.com/10km/sql2java 
// JDBC driver used at code generation time: com.mysql.jdbc.Driver
// template: metadata.java.vm
// ______________________________________________________
package net.gdface.facelog.db;

import java.util.List;
import java.util.Arrays;
import gu.sql2java.RowMetaData;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableList;

/**
 * Class to supply table meta data for the fl_device table.<br>
 * @author guyadong
 */
public class DeviceMetaData extends RowMetaData implements Constant
{  
    private static final ImmutableMap<String, String> JUNCTION_TABLE_PK_MAP = ImmutableMap.of();    
    private static final ImmutableList<String> FOREIGN_KEYS = 
        ImmutableList.<String>builder()
            .add("fl_device_ibfk_1 (group_id) REFERENCES fl_device_group(id) RESTRICT SET_NULL")
            .build();
    private static final ImmutableList<String> IMPORTED_FKNAMES = 
        ImmutableList.<String>builder()
            .add("fl_error_log_ibfk_2(fl_error_log)") 
            .add("fl_image_ibfk_1(fl_image)") 
            .add("fl_log_ibfk_2(fl_log)") 
            .build();
    private static final ImmutableList<String> INDEXS = 
        ImmutableList.<String>builder()
            .add("mac (mac) UNIQUE")
            .add("serial_no (serial_no) UNIQUE")
            .add("group_id (group_id) ")
            .build();

    private static final List<String> GETTERS = Arrays.asList("getId","getGroupId","getName","getProductName","getModel","getVendor","getManufacturer","getMadeDate","getVersion","getUsedSdks","getSerialNo","getMac","getDirection","getRemark","getExtBin","getExtTxt","getCreateTime","getUpdateTime","getStatus","getOptions");

    private static final List<String> SETTERS = Arrays.asList("setId","setGroupId","setName","setProductName","setModel","setVendor","setManufacturer","setMadeDate","setVersion","setUsedSdks","setSerialNo","setMac","setDirection","setRemark","setExtBin","setExtTxt","setCreateTime","setUpdateTime","setStatus","setOptions");

    private static final String AUTO_INCREMENT_COLUMN = "id";

    private static final Class<?> LOCK_COLUMN_TYPE = null;
    private static final String LOCK_COLUMN_NAME = null;
    public DeviceMetaData(){
        super(
            "fl_device",
            "TABLE",
            DeviceBean.class,
            "Device",
            IDeviceManager.class,
            ALIAS_NAME,
            FL_DEVICE_FIELDS_LIST,
            FL_DEVICE_JAVA_FIELDS_LIST,
            GETTERS,
            SETTERS,
            FL_DEVICE_FIELD_TYPES,
            FL_DEVICE_FIELD_SIZES,
            FL_DEVICE_FIELD_SQL_TYPES,
            FL_DEVICE_PK_FIELDS_LIST,
            JUNCTION_TABLE_PK_MAP,
            LOCK_COLUMN_TYPE,
            LOCK_COLUMN_NAME,
            FOREIGN_KEYS,
            IMPORTED_FKNAMES,
            INDEXS,
            AUTO_INCREMENT_COLUMN
        );
    }
}
