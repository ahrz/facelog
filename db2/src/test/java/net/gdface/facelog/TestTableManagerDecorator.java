package net.gdface.facelog;

import org.junit.BeforeClass;
import org.junit.Test;

import gu.sql2java.Managers;
import gu.sql2java.TableManager;
import static gu.sql2java.SimpleLog.*;

import java.lang.reflect.Proxy;
public class TestTableManagerDecorator {
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Managers.setDebug(true);
	}
	@Test
	public void test1() {
		try {
				TableManager<?> manager = Managers.getTableManager("fl_person");
//				log("{}", manager);
				log("{}",Proxy.getInvocationHandler(manager));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	public void test2() {
		try {
			for(String tablename:new String[]{"fl_log_light","fl_log","fl_face","fl_feature","fl_person","fl_image","fl_device","fl_permit","fl_person_group","fl_device_group","fl_store"}){
				TableManager<?> manager = Managers.getTableManager(tablename);
//				log("{}", manager);
				log("{}",Proxy.getInvocationHandler(manager));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
