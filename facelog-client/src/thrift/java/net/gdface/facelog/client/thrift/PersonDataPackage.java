package net.gdface.facelog.client.thrift;

import com.facebook.swift.codec.*;
import com.facebook.swift.codec.ThriftField.Requiredness;
import java.util.*;

import static com.google.common.base.Objects.toStringHelper;

@ThriftStruct("PersonDataPackage")
public final class PersonDataPackage
{
    public PersonDataPackage() {
    }

    private List<FeatureBean> featureBeans;

    @ThriftField(value=1, name="featureBeans", requiredness=Requiredness.OPTIONAL)
    public List<FeatureBean> getFeatureBeans() { return featureBeans; }

    @ThriftField
    public void setFeatureBeans(final List<FeatureBean> featureBeans) { this.featureBeans = featureBeans; }

    private String limit;

    @ThriftField(value=2, name="limit", requiredness=Requiredness.OPTIONAL)
    public String getLimit() { return limit; }

    @ThriftField
    public void setLimit(final String limit) { this.limit = limit; }

    private PersonBean personBean;

    @ThriftField(value=3, name="personBean", requiredness=Requiredness.OPTIONAL)
    public PersonBean getPersonBean() { return personBean; }

    @ThriftField
    public void setPersonBean(final PersonBean personBean) { this.personBean = personBean; }

    private String schedule;

    @ThriftField(value=4, name="schedule", requiredness=Requiredness.OPTIONAL)
    public String getSchedule() { return schedule; }

    @ThriftField
    public void setSchedule(final String schedule) { this.schedule = schedule; }

    @Override
    public String toString()
    {
        return toStringHelper(this)
            .add("featureBeans", featureBeans)
            .add("limit", limit)
            .add("personBean", personBean)
            .add("schedule", schedule)
            .toString();
    }
}
