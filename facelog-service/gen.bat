@echo off
SETLOCAL
set sh_folder=%~dp0
pushd %sh_folder%
call gen-decorator-service.bat && ^
call gen-decorator-service-mini.bat && ^
call mvn install && ^
call gen_thrift.bat && ^
call gen_thrift_mini.bat && ^
call gen_erpc.bat && ^
call gen_erpc_mini.bat && ^
call gen_client.bat && ^
call gen_client-thrifty.bat && ^
call gen-client-ext.bat && ^
call gen-decorator-client.bat && ^
call gen-decorator-client-thrifty && ^
popd
ENDLOCAL