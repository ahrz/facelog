#!/bin/bash
# 生成 facelog 接口定义文件(IDL)
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd $sh_folder
mvn com.gitee.l0km:swift2thrift-maven-plugin:generate@thrift $* || exit
popd