#!/bin/bash
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd $sh_folder > /dev/null 2>&1
java $FACELOG_OPTS $FACELOG_DEBUG_OPTS -jar ${project.build.finalName}-standalone.jar $*
popd  > /dev/null 2>&1