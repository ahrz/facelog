#!/bin/bash
# 生成 FaceLog C client代码脚本
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd "$sh_folder"
IDL=$sh_folder/IFaceLog.thrift
[ -n "$1" ] && IDL="$1"
OUT_FOLDER=$sh_folder/../facelog-client-ext/c
[ -n "$2" ] && OUT_FOLDER="$2" 
# 指定thrift compiler位置
[ $(which thrift) >/dev/null ] && THRIFT_EXE=thrift
[ -z "$THRIFT_EXE" ] && THRIFT_EXE=$sh_folder/dependencies/dist/thrift-$(g++ -dumpmachine)/bin/thrift
$THRIFT_EXE --version || exit

[ -e "$OUT_FOLDER" ] && rm "$OUT_FOLDER/*.c" >/dev/null 2>/dev/null
[ ! -e "$OUT_FOLDER" ] && rm -fr "$OUT_FOLDER"

$THRIFT_EXE --gen c_glib \
	-out "$OUT_FOLDER" \
	"$IDL" || exit 

popd
