#!/bin/bash
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd $sh_folder
./gen-decorator-service.sh && \
./gen-decorator-service-mini.sh && \
mvn install && \
./gen_thrift.sh && \
./gen_thrift_mini.sh && \
./gen_erpc.sh && \
./gen_erpc_mini.sh && \
./gen_client.sh && \
./gen_client-thrifty.sh && \
./gen-client-ext.sh && \
./gen-decorator-client.sh && \
./gen-decorator-client-thrifty.sh
popd
