#!/bin/bash
# 生成基于facebook/swift的client端接口实现代码
sh_folder=$(cd "$(dirname $0)"; pwd -P)
pushd "$sh_folder/../facelog-client"
OUT_FOLDER=src/codegen/java
[ -e "$OUT_FOLDER" ] && rm -fr "$OUT_FOLDER"
mvn com.gitee.l0km:codegen-thrift-maven-plugin:generate || exit
popd