echo off
rem 生成 FaceLog C client代码脚本
set sh_folder=%~dp0
rem 删除最后的 '\'
set sh_folder=%sh_folder:~0,-1%
pushd "%sh_folder%"
set IDL=%sh_folder%\IFaceLog.thrift
if NOT "%1"=="" ( set IDL=%1 )
set OUT_FOLDER=%sh_folder%\..\facelog-client-ext\c
if NOT "%2"=="" ( set OUT_FOLDER=%2 )
:: 指定thrift compiler位置
where thrift >nul 2>nul
if %ERRORLEVEL% == 0 (
	set THRIFT_EXE=thrift
	goto :gen
	)
set THRIFT_EXE=%sh_folder%\dependencies\dist\thrift-dev\vc140\x64\bin\thrift.exe
if not exist "%THRIFT_EXE%" (
	echo "not found thrift.exe,please build thrift"
	exit /B -1
	)
	
:gen
if exist "%OUT_FOLDER%" (
	del  "%OUT_FOLDER%"\*.c >nul 2>nul
	)
if not exist "%OUT_FOLDER%" mkdir  "%OUT_FOLDER%"

%THRIFT_EXE% --gen c_glib^
	-out "%OUT_FOLDER%" ^
	"%IDL%"

popd
