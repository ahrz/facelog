package net.gdface.facelog.client;

import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.Map;

import com.google.common.base.Supplier;
import gu.dtalk.MenuItem;
import gu.dtalk.cmd.CmdManager;
import gu.dtalk.cmd.TaskManager;
import gu.dtalk.engine.BaseDispatcher;
import gu.dtalk.engine.TaskDispatcher;
import gu.simplemq.IMessageAdapter;
import gu.simplemq.IMessageQueueFactory;
import net.gdface.facelog.IFaceLog;
import net.gdface.facelog.IFaceLogDecorator;
import net.gdface.facelog.MQParam;
import net.gdface.facelog.Token;
import net.gdface.facelog.client.ClientExtendTools.ParameterSupplier;
import net.gdface.facelog.client.dtalk.DtalkEngineForFacelog;
import net.gdface.facelog.hb.DynamicChannelListener;
import net.gdface.facelog.hb.DeviceHeartbeat;
import net.gdface.facelog.mq.ServiceHeartbeatListener;
import net.gdface.facelog.thrift.IFaceLogThriftClient;

public class IFaceLogClient extends IFaceLogDecorator {
	public final ClientExtendTools clientTools;
	public IFaceLogClient(IFaceLog delegate) {
		super(delegate);
		clientTools = new ClientExtendTools(delegate);
	}
    /**
     * @param host RPC service host
     * @param port RPC service port 
     */
    public IFaceLogClient(String host,int port) {
        this(new IFaceLogThriftClient(host,port));
    }
    /**
     * test if connectable for RPC service
     * @return return {@code true} if connectable ,otherwise {@code false}
     */
    public boolean testConnect(){
        return clientTools.getFactory().testConnect();
    }
	/**
	 * 如果{@code host}是本机地址则用facelog服务主机名替换
	 * @param host
	 * @return  valid host name 
	 */
	public String insteadHostIfLocalhost(String host) {
		return clientTools.insteadHostIfLocalhost(host);
	}
	/**
	 * 如果{@code uri}的主机名是本机地址则用facelog服务主机名替换
	 * @param uri
	 * @return {@code uri} or new URI instead with host of facelog
	 */
	public URI insteadHostIfLocalhost(URI uri) {
		return clientTools.insteadHostIfLocalhost(uri);
	}
	/**
	 * 如果{@code url}的主机名是本机地址则用facelog服务主机名替换
	 * @param url
	 * @return {@code url} or new URI instead with host of facelog
	 */
	public URL insteadHostIfLocalhost(URL url) {
		return clientTools.insteadHostIfLocalhost(url);
	}
	/**
	 * @param deviceId
	 * @return Supplier instance
	 * @see net.gdface.facelog.client.ClientExtendTools#getDeviceGroupIdSupplier(int)
	 */
	public Supplier<Integer> getDeviceGroupIdSupplier(int deviceId) {
		return clientTools.getDeviceGroupIdSupplier(deviceId);
	}
	/**
	 * @param personId
	 * @return Supplier instance
	 * @see net.gdface.facelog.client.ClientExtendTools#getPersonGroupBelonsSupplier(int)
	 */
	public Supplier<List<Integer>> getPersonGroupBelonsSupplier(int personId) {
		return clientTools.getPersonGroupBelonsSupplier(personId);
	}
	/**
	 * @param token
	 * @return CmdManager instance
	 * @see net.gdface.facelog.client.ClientExtendTools#makeCmdManager(net.gdface.facelog.Token)
	 */
	public CmdManager makeCmdManager(Token token) {
		return clientTools.makeCmdManager(token);
	}	
	/**
	 * @param token
	 * @param cmdpath 设备(菜单)命令路径
	 * @param taskQueueSupplier
	 * @return TaskManager instance
	 * @see net.gdface.facelog.client.ClientExtendTools#makeTaskManager(Token, String, Supplier)
	 */
	public TaskManager makeTaskManager(Token token, String cmdpath, Supplier<String> taskQueueSupplier) {
		return clientTools.makeTaskManager(token, cmdpath, taskQueueSupplier);
	}
	/**
	 * @param token
	 * @return BaseDispatcher instance
	 * @see net.gdface.facelog.client.ClientExtendTools#makeCmdDispatcher(net.gdface.facelog.Token)
	 */
	public BaseDispatcher makeCmdDispatcher(Token token) {
		return clientTools.makeCmdDispatcher(token);
	}
	/**
	 * @param token
	 * @param taskQueueSupplier
	 * @return TaskDispatcher instance
	 * @see net.gdface.facelog.client.ClientExtendTools#makeTaskDispatcher(Token, Supplier)
	 */
	public TaskDispatcher makeTaskDispatcher(Token token, Supplier<String> taskQueueSupplier) {
		return clientTools.makeTaskDispatcher(token, taskQueueSupplier);
	}
	/**
	 * @param duration
	 * @param token
	 * @return Supplier instance
	 * @see net.gdface.facelog.client.ClientExtendTools#getAckChannelSupplier(int, net.gdface.facelog.Token)
	 */
	public Supplier<String> getAckChannelSupplier(int duration, Token token) {
		return clientTools.getAckChannelSupplier(duration,token);
	}
	/**
	 * @param token
	 * @return Supplier instance
	 * @see net.gdface.facelog.client.ClientExtendTools#getAckChannelSupplier(net.gdface.facelog.Token)
	 */
	public Supplier<String> getAckChannelSupplier(Token token) {
		return clientTools.getAckChannelSupplier(token);
	}
	/**
	 * @param token
	 * @return Supplier instance
	 * @see net.gdface.facelog.client.ClientExtendTools#getCmdSnSupplier(net.gdface.facelog.Token)
	 */
	public Supplier<Integer> getCmdSnSupplier(Token token) {
		return clientTools.getCmdSnSupplier(token);
	}
	/**
	 * @param deviceToken
	 * @param rootMenu
	 * @return DtalkEngineForFacelog instance
	 * @see net.gdface.facelog.client.ClientExtendTools#initDtalkEngine(net.gdface.facelog.Token, gu.dtalk.MenuItem)
	 */
	public DtalkEngineForFacelog initDtalkEngine(Token deviceToken, MenuItem rootMenu) {
		return clientTools.initDtalkEngine(deviceToken, rootMenu);
	}
	/**
	 * @param token
	 * @see net.gdface.facelog.client.ClientExtendTools#initMQDefaultFactory(net.gdface.facelog.Token)
	 */
	public void initMQDefaultFactory(Token token) {
		clientTools.initMQDefaultFactory(token);
	}
	/**
	 * 转换参数中的主机名
	 * @see net.gdface.facelog.IFaceLogDecorator#getRedisParameters(net.gdface.facelog.Token)
	 * @see ClientExtendTools#insteadHostOfMQParamIfLocalhost(Map)
	 */
	@Override
	public Map<MQParam, String> getRedisParameters(Token token) {
		Map<MQParam, String> parameters = super.getRedisParameters(token);
		return clientTools.insteadHostOfMQParamIfLocalhost(parameters);
	}
	/**
	 * @param token
	 * @return 返回一个获取redis参数的{@link Supplier}实例
	 * @see net.gdface.facelog.client.ClientExtendTools#getRedisParametersSupplier(net.gdface.facelog.Token)
	 */
	public Supplier<Map<MQParam, String>> getRedisParametersSupplier(Token token) {
		return clientTools.getRedisParametersSupplier(token);
	}
	/**
	 * @param token
	 * @return 返回一个获取消息系统参数的{@link Supplier}实例
	 * @see net.gdface.facelog.client.ClientExtendTools#getMessageQueueParametersSupplier(net.gdface.facelog.Token)
	 */
	public Supplier<Map<MQParam, String>> getMessageQueueParametersSupplier(Token token) {
		return clientTools.getMessageQueueParametersSupplier(token);
	}
	/**
	 * 转换参数中的主机名
	 * @see net.gdface.facelog.IFaceLogDecorator#getFaceApiParameters(net.gdface.facelog.Token)
	 * @see ClientExtendTools#insteadHostOfValueIfLocalhost(Map)
	 */
	@Override
	public Map<String, String> getFaceApiParameters(Token token) {
		Map<String, String> parameters = super.getFaceApiParameters(token);
		return clientTools.insteadHostOfValueIfLocalhost(parameters);
	}
	/**
	 * @param token
	 * @return 返回一个获取faceapi参数的{@link Supplier}实例
	 * @see net.gdface.facelog.client.ClientExtendTools#getFaceApiParametersSupplier(net.gdface.facelog.Token)
	 */
	public Supplier<Map<String, String>> getFaceApiParametersSupplier(Token token) {
		return clientTools.getFaceApiParametersSupplier(token);
	}
	/**
	 * @param token
	 * @return 返回一个获取设备心跳实时监控通道名的{@link Supplier}实例
	 * @see net.gdface.facelog.client.ClientExtendTools#getDynamicParamSupplier(MQParam mqParam, Token token)
	 * @deprecated replaced by {@link #getDynamicParamSupplier(MQParam, Token)}
	 */
	public Supplier<String> getMonitorChannelSupplier(Token token) {
		return clientTools.getDynamicParamSupplier(MQParam.HB_MONITOR_CHANNEL,token);
	}
	/**
	 * @param mqParam 
	 * @param token 访问令牌
	 * @return 返回一个动态获取指定消息参数的{@link Supplier}实例
	 */
	public Supplier<String> getDynamicParamSupplier(MQParam mqParam,Token token){
		return clientTools.getDynamicParamSupplier(mqParam,token);
	}
	/**
	 * 返回有效令牌的{@link Supplier}实例<br>
	 * @return {@link Supplier}实例
	 */
	public Supplier<Token> getTokenSupplier() {
		return clientTools.getTokenSupplier();
	}
	/**
	 * 添加服务心跳侦听器
	 * @param listener
	 * @return 当前{@link IFaceLogClient}对象
	 */
	public IFaceLogClient addServiceEventListener(ServiceHeartbeatListener listener){
		clientTools.addServiceEventListener(listener);	
		return this;
	}
	/**
	 * 删除服务心跳侦听器
	 * @param listener
	 * @return 当前{@link IFaceLogClient}对象
	 */
	public IFaceLogClient removeServiceEventListener(ServiceHeartbeatListener listener){
		clientTools.removeServiceEventListener(listener);
		return this;
	}

	/**
	 * 创建动态频道名侦听对象
	 * 动态频道是指定当服务重启后，频道名会动态改变的频道，
	 * 对于这种频道，通过侦听服务心跳判断服务是否重启，如果重启则重新获取频道名继续保持侦听
	 * @param listener
	 * @param channelType 频道消息数据类型
	 * @param mqParam 参数名
	 * @param token
	 * @param factory 消息系统工厂类实例
	 * @return 返回{@link DynamicChannelListener}实例
	 */
	public <T>DynamicChannelListener<T> makeDynamicChannelListener(IMessageAdapter<T> listener,Class<T> channelType, MQParam mqParam, Token token, IMessageQueueFactory factory) {
		return clientTools.makeDynamicChannelListener(listener, channelType, mqParam, token, factory);
	}
	/**
	 * 创建设备心跳包发送对象<br>
	 * {@link DeviceHeartbeat}为单实例,该方法只能调用一次
	 * @param deviceID 设备ID
	 * @param token 设备令牌
	 * @return {@link DeviceHeartbeat}实例
	 */
	public DeviceHeartbeat makeHeartbeat(int deviceID, Token token) {
		return clientTools.makeHeartbeat(deviceID, token);
	}
	/**
	 * @param tokenHelper 要设置的 tokenHelper
	 * @return 当前{@link IFaceLogClient}实例
	 */
	public IFaceLogClient setTokenHelper(TokenHelper tokenHelper) {
		clientTools.setTokenHelper(tokenHelper);
		return this;
	}
	/**
	 * 启动服务心跳侦听器<br>
	 * 启动侦听器后CLIENT端才能感知服务端断线，并执行相应动作。
	 * 调用前必须先执行{@link #setTokenHelper(TokenHelper)}初始化
	 * @param token 令牌
	 * @param initMQDefaultFactoryInstance 是否初始化 {@link IMessageQueueFactory}默认实例
	 * @return 返回当前{@link IFaceLogClient}实例
	 */
	public IFaceLogClient startServiceHeartbeatListener(Token token, boolean initMQDefaultFactoryInstance) {
		clientTools.startServiceHeartbeatListener(token, initMQDefaultFactoryInstance);
		return this;
	}
	/**
	 * @param task
	 * @param token
	 * @return ParameterSupplier instance
	 * @see net.gdface.facelog.client.ClientExtendTools#getTaskQueueSupplier(java.lang.String, net.gdface.facelog.Token)
	 */
	public ParameterSupplier<String> getTaskQueueSupplier(String task, Token token) {
		return clientTools.getTaskQueueSupplier(task, token);
	}
	/**
	 * @param task
	 * @param sdkVersion
	 * @param token
	 * @return ParameterSupplier instance
	 * @see net.gdface.facelog.client.ClientExtendTools#getSdkTaskQueueSupplier(java.lang.String, java.lang.String, net.gdface.facelog.Token)
	 */
	public ParameterSupplier<String> getSdkTaskQueueSupplier(String task, String sdkVersion, Token token) {
		return clientTools.getSdkTaskQueueSupplier(task, sdkVersion, token);
	}
}
